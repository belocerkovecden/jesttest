## Тестирование кода javascript

Результаты покрытия будут в директории coverage/Icov-report

---

- Ставим Jest глобально

```
npm i jest -g
```

- Создаём файл package.json локально

```
npm init
```

- Запускаем тесты

```
jest
```

- Запускаем тесты с проверкой покрытия

```
jest --coverage
```