const Calc = require('../src/Calc.js');
const calc = new Calc();

test('Нормальный тест', () => {
	expect(calc.sum(2, 5)).toBe(7);
});

test('div', () => {
   expect(calc.div(10, 5)).toBe(2);
   expect(calc.div(10, 0)).toBe(10);
   expect(calc.div(10)).toBe(10);
   expect(calc.div(10, 'sdfdfdf')).toBe(10);
});

test('Простойт тест', () => {
	expect(20).toBe(20);
	expect(50).toBe(50);
});

test('Ещё один простойт тест', () => {
	expect(30000).toBe(30000);
});

describe('div - describe', () => {
   test('деление', () => {
       expect(calc.div(10, 5)).toBe(2);
   });
 
   test('на 0', () => {
       expect(calc.div(10, 0)).toBe(10);
   });
 
   test('без второго аргумента', () => {
       expect(calc.div(10)).toBe(10);
   });
 
   test('на не число', () => {
       expect(calc.div(10, 'sdfdfdf')).toBe(10);
   });
 
});